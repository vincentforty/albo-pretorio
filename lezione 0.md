Lezione 0:

* Usiamo un editor, nel corso usano Visual Studio Code (code.visualstudio.com) che ha anche dei plugin, ma potete usare qualsiasi editor (anche nano);
* Ci serve naturalmente un browser (Firefox, Chrome, Safari ecc.);
* scarichiamo nodejs da nodejs.org (la versione LTS) o da riga di comando;

io ho dato:

```
sudo apt-get update
sudo apt-get install nodejs
```

- poi installiamo npm:

```
sudo apt-get install npm
```

per visualizzare la versione e controllare che sia andato tutto ok, diamo: 

```
nodejs -v
```

e

```
npm -v
```

- creiamo una cartella Example 

```
mkdir Example
cd Example
npm init
```

ora inseriamo package name: example
poi dare sempre invio e rispondere yes alla domanda "is it ok?"

Vedremo il package.json dentro la nostra cartella Example.
Ora dentro Example creiamo il nostro primo file index.js.
